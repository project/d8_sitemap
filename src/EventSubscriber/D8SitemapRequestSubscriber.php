<?php

namespace Drupal\d8_sitemap\EventSubscriber;

use Drupal\d8_sitemap\D8SitemapResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Stop the request processing before the controller is called.
 */
class D8SitemapRequestSubscriber implements EventSubscriberInterface {

  /**
   * @var bool
   */
  protected $status = FALSE;

  public function on() {
    $this->status = TRUE;
  }

  public function set(RequestEvent $event) {
    if ($this->status) {
      $event->setResponse(new D8SitemapResponse());
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = 'set';
    return $events;

  }
}
