<?php

namespace Drupal\d8_sitemap\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\HtmlResponse;
use Drupal\d8_sitemap\D8SitemapHelper;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\TerminateEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class D8SitemapTerminateSubscriber implements EventSubscriberInterface {

  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->storage = $entityTypeManager->getStorage('d8_sitemap');
  }

  public function onTerminate(TerminateEvent $event) {
    if (PHP_SAPI === 'cli' || !$event->isMasterRequest()) {
      return;
    }
    $response = $event->getResponse();
    $path = $event->getRequest()->getRequestUri();
    if (false !== $pos = strpos($path, '?')) {
      $path = substr($path, 0, $pos);
    }

    switch (strval($response->getStatusCode())[0]) {
      case 2:
        if ($response instanceof HtmlResponse && strpos($response->headers->get('Content-Type'), 'text/html') !== FALSE) {
          D8SitemapHelper::ensurePathExists($this->storage, $path);
        }
        break;
      case 3:
      case 4:
        D8SitemapHelper::ensurePathNotExists($this->storage, $path);
        break;
    }
  }

  public static function getSubscribedEvents() {
    return [KernelEvents::TERMINATE => 'onTerminate'];
  }

}
