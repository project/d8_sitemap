<?php

namespace Drupal\d8_sitemap\Entity;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Language\Language;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\d8_sitemap\D8SitemapHelper;

/**
 * Defines the D8 sitemap entity class.
 *
 * @ContentEntityType(
 *   id = "d8_sitemap",
 *   label = @Translation("D8 sitemap"),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\Core\Entity\ContentEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "storage_schema" = "Drupal\d8_sitemap\D8SitemapStorageSchema",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 *   admin_permission = "administer d8 sitemap",
 *   base_table = "d8_sitemap",
 *   entity_keys = {
 *     "id" = "item_id",
 *     "label" = "path",
 *   },
 *   links = {
 *     "add-form" = "/d8_sitemap/add",
 *     "delete-form" = "/d8_sitemap/{d8_sitemap}/delete",
 *     "collection" = "/admin/content/d8_sitemap",
 *   }
 * )
 */
class D8Sitemap extends ContentEntityBase {

  /**
   * Number of maximum failures.
   */
  const MAXFAILURES = 5;

  /**
   * @var \Drupal\Core\Language\LanguageInterface[]
   */
  protected $availableLanguages = [];

  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    if (isset($values['path']) && $values['path'][0] !== '!') {
      $values['path'] = D8SitemapHelper::getCanonicalPath($values['path']);
    }
    parent::preCreate($storage, $values);
  }

  public function getPath() {
    return $this->path->value;
  }

  public function setPath($path) {
    $this->path->value = $path;
    return $this;
  }

  public function setAvailableLanguages(array $languages) {
    $this->availableLanguages  = $languages;
    return $this;
  }

  public function getAvailableLanguages() {
    return $this->availableLanguages ?: [\Drupal::service('language.default')->get()];
  }

  /**
   * Reset the number of failures.
   *
   * @return $this
   */
  public function resetNumberOfFailures() {
    // "created" stores the number of failures.
    $this->created->value = 0;
    return $this;
  }

  /**
   * Handle a failure.
   */
  public function handleFailure() {
    if (intval($this->created->value) === self::MAXFAILURES) {
      $this->delete();
    }
    else {
      if ($this->created->value > self::MAXFAILURES) {
        $this->created->value = 1;
      }
      else {
        $this->created->value++;
      }
      $this->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields['path'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Path'));
    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel('Queue name')
      ->setSetting('max_length', 10)
      ->setDefaultValue('d8_sitemap')
      ->setDisplayOptions('form', [
        'region' => 'hidden',
      ]);
    $fields['data'] = BaseFieldDefinition::create('string_long')
      ->setLabel('not used')
      ->setSetting('case_sensitive', TRUE)
      ->setDefaultValue('N;');
    $fields['expire'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('expire'))
      ->setDefaultValue(0)
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('form', [
        'region' => 'hidden',
      ]);
    $fields['created'] = BaseFieldDefinition::create('integer')
      ->setLabel('created')
      ->setDisplayOptions('form', [
        'region' => 'hidden',
      ]);
    $fields['xml'] = BaseFieldDefinition::create('string_long')
      ->setLabel('The XML chunk')
      ->setDisplayOptions('form', [
        'region' => 'hidden',
      ]);
    return $fields;
  }

  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    if (!\Drupal::database()->queryRange('SELECT * FROM {users_field_data}', 0, 1)->fetchAll()) {
      exit;
    }
    parent::preDelete($storage, $entities);
  }

}
