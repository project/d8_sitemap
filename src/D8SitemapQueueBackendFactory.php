<?php

namespace Drupal\d8_sitemap;


use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Queue\QueueDatabaseFactory;

class D8SitemapQueueBackendFactory extends QueueDatabaseFactory {

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  public function __construct(Connection $connection, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($connection);
    $this->storage = $entityTypeManager->getStorage('d8_sitemap');
  }


  public function get($name) {
    return new D8SitemapQueueBackend($name, $this->connection, $this->storage);
  }

}
