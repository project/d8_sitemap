<?php

namespace Drupal\d8_sitemap;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\d8_sitemap\Entity\D8Sitemap;

class D8SitemapHelper {

  public static function queuePathForProcessing(EntityStorageInterface $storage, string $path) {
    if ($sitemap = self::ensurePathExists($storage, $path)) {
      $sitemap->expire->value = 0;
      $sitemap->save();
    }
  }

  /**
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   * @param string $path
   *
   * @return \Drupal\d8_sitemap\Entity\D8Sitemap|null
   *   The sitemap if one existed. If a new one was created, it is not
   *   returned because new ones do not need be reset for processing.
   */
  public static function ensurePathExists(EntityStorageInterface $storage, string $path): ?D8Sitemap {
    /** @var \Drupal\d8_sitemap\Entity\D8Sitemap[] $sitemaps */
    if ($sitemaps = $storage->loadByProperties(['path' => $path])) {
      return reset($sitemaps);
    }
    $storage->create([
      'path' => $path,
      'created' => 0,
    ])->save();
    return NULL;
  }

  public static function ensurePathNotExists(EntityStorageInterface $storage, string $path) {
    if ($sitemaps = $storage->loadByProperties(['path' => $path])) {
      $storage->delete($sitemaps);
    }
  }

  public static function isOurEntityType(EntityTypeInterface $entityType): bool {
    // Too many entities like block content have canonical  templates so
    // filtering on that is not enough. Core, however, has only media, node,
    // term, media and user for common reference targets which is ideal.
    return
      $entityType->hasLinkTemplate('canonical') &&
      $entityType->isCommonReferenceTarget() &&
      $entityType->getBaseTable() &&
      $entityType->getKey('id');
  }

  /**
   * @param $path
   *
   * @return string
   */
  public static function getCanonicalPath($path): string {
    if (UrlHelper::isExternal($path)) {
      // Keep only local parts.
      $parts = parse_url($path);
      $path = $parts['path'] ?? '';
      $query = !empty($parts['query']) ? '?' . $parts['query'] : '';
      $fragment = !empty($parts['fragment']) ? '#' . $parts['fragment'] : '';
      $path = $path . $query . $fragment;
    }
    return '/' . ltrim($path, '/');
  }

}
