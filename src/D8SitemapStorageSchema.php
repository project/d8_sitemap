<?php

namespace Drupal\d8_sitemap;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;

class D8SitemapStorageSchema extends SqlContentEntityStorageSchema {

  /**
   * {@inheritdoc}
   */
  protected function getEntitySchema(ContentEntityTypeInterface $entity_type, $reset = FALSE) {
    $schema = parent::getEntitySchema($entity_type, $reset);

    $schema['d8_sitemap']['indexes'] += [
      'name_created' => ['name', 'created'],
      'expire' => ['expire'],
    ];

    return $schema;
  }

}
