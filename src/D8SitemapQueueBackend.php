<?php

namespace Drupal\d8_sitemap;


use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Queue\DatabaseQueue;
use Drupal\d8_sitemap\Entity\D8Sitemap;

class D8SitemapQueueBackend extends DatabaseQueue {

  const TABLE_NAME = 'd8_sitemap';

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  public function __construct($name, Connection $connection, EntityStorageInterface $storage) {
    parent::__construct($name, $connection);
    $this->storage = $storage;
  }


  /**
   * @return FALSE|\Drupal\d8_sitemap\Entity\D8Sitemap
   */
  public function claimEntity($lease_time = 300) {
    $entity = FALSE;
    $item = $this->claimItem($lease_time);
    if ($item) {
      // loadUnchanged() is necessary because claimItem() updates the database
      // directly.
      /** @var D8Sitemap $entity */
      $entity = $this->storage->loadUnchanged($item->item_id);
    }
    return $entity;
  }

  public function deleteItem($item) {
    // Thanks, but no.
  }

  public function deleteQueue() {
    // Thanks, but no.
  }

}
