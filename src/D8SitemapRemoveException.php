<?php

namespace Drupal\d8_sitemap;

use Drupal\d8_sitemap\Entity\D8Sitemap;

class D8SitemapRemoveException extends \Exception {

  /**
   * @var \Drupal\d8_sitemap\Entity\D8Sitemap
   */
  protected $sitemap;

  public static function createFromSitemap(D8Sitemap $sitemap, $message = '') {
    $exception = new static($message);
    $exception->sitemap = $sitemap;
    return $exception;
  }

  public function getSitemap() {
    return $this->sitemap;
  }

}
