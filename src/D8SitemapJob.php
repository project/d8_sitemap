<?php

namespace Drupal\d8_sitemap;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Render\HtmlResponse;
use Drupal\Core\Routing\AccessAwareRouterInterface;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Session\AnonymousUserSession;
use Drupal\Core\Url;
use Drupal\d8_sitemap\Entity\D8Sitemap;
use Drupal\d8_sitemap\EventSubscriber\D8SitemapRequestSubscriber;
use Drupal\metatag\MetatagManagerInterface;
use RobotsTxtParser\RobotsTxtParser;
use RobotsTxtParser\RobotsTxtValidator;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class D8SitemapJob {

  /**
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $httpKernel;

  /**
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * @var \Symfony\Component\HttpFoundation\Session\SessionInterface
   */
  protected $session;

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $accountProxy;

  /**
   * @var \Drupal\d8_sitemap\EventSubscriber\D8SitemapRequestSubscriber
   */
  protected $subscriber;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\metatag\MetatagManagerInterface
   */
  protected $metatagManager;

  /**
   * @var \RobotsTxtParser\RobotsTxtValidator
   */
  protected $validator;

  /**
   * @var int
   */
  protected int $recursionDepth;

  public function __construct(QueueFactory $queueFactory, HttpKernelInterface $httpKernel, LanguageManagerInterface $languageManager, EntityTypeManagerInterface $entityTypeManager, SessionInterface $session, AccountProxyInterface $accountProxy, MetatagManagerInterface $metatagManager, D8SitemapRequestSubscriber $subscriber) {
    $this->queueFactory = $queueFactory;
    $this->httpKernel = $httpKernel;
    $this->languageManager = $languageManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->session = $session;
    $this->accountProxy = $accountProxy;
    $this->metatagManager = $metatagManager;
    $this->subscriber = $subscriber;
  }


  /**
   * Generate the sitemap.
   */
  public function generate() {
    \Drupal::database()->query('SET SESSION MAX_EXECUTION_TIME = 1000');
    \Drupal::service('page_cache_kill_switch')->trigger();
    $leaseTime = 50;
    $end = time() + $leaseTime;
    $queue = $this->queueFactory->get('d8_sitemap');
    if (!$queue instanceof D8SitemapQueueBackend) {
      return;
    }
    $queue->garbageCollection();
    // Although there's no reason drush would be logged in, enforce anonymous.
    $account = new AnonymousUserSession();
    $this->accountProxy->setAccount($account);
    $this->session->set('uid', $account->id());
    // Switch our subscriber on.
    $this->subscriber->on();
    while (time() < $end && ($sitemap = $queue->claimEntity($leaseTime))) {
      try {
        $this->recursionDepth = 0;
        $sitemap = $this->processSitemap($sitemap);
        // We are finished with this one, this expire ensures neither
        // claimItem() nor garbageCollection() will pick it up.
        $sitemap->expire->value = 2147483647;
        $sitemap
          ->resetNumberOfFailures()
          ->save();
      }
      catch (D8SitemapRemoveException $e) {
        $e->getSitemap()->handleFailure();
      }
    }
  }

  /**
   * @param \Drupal\d8_sitemap\Entity\D8Sitemap $sitemap
   *
   * @return \Drupal\d8_sitemap\Entity\D8Sitemap
   */
  protected function processSitemap(D8Sitemap $sitemap) {
    if ($this->recursionDepth++ > 50) {
      throw D8SitemapRemoveException::createFromSitemap($sitemap);
    }
    $path = $sitemap->getPath();
    if ($path[0] === '!') {
      return $this->processNewEntityPath($sitemap, $path);
    }
    $this->checkRobotsTxt($sitemap, $path);
    $request = Request::create($GLOBALS['base_url'] . $path);
    // Redirect won't fire if this is not set.
    $request->server->set('SCRIPT_NAME', '/index.php');
    $response = $this->httpKernel->handle($request);
    $return = $this->processResponse($sitemap, $response);
    if ($return) {
      return $return;
    }
    $attributes = $request->attributes;
    $this->checkAccess($sitemap, $attributes);
    $this->checkException($sitemap, $attributes);
    // Url::createFromRequest() uses the no access check router which is not
    // what we want and it does not run middlewares, request subscribers etc.
    // so go through a route match instead. It is useful for getting the route
    // name and parameters, too.
    $routeMatch = RouteMatch::createFromRequest($request);
    $routeName = $routeMatch->getRouteName();
    // Try to find an entity and check whether this path is an unprocessed
    // entity URL and if so then move to the processed URL if it is different.
    foreach ($routeMatch->getParameters() as $key => $value) {
      if ($routeName === "entity.$key.canonical" && $value instanceof ContentEntityInterface) {
        $return = $this->processCanonicalEntityPath($sitemap, $value, $path);
        if ($return) {
          return $return;
        }
      }
    }
    $sitemap->xml->value = $this->getXml($routeMatch, $sitemap->getAvailableLanguages());
    return $sitemap;
  }

  /**
   * @param \Drupal\d8_sitemap\Entity\D8Sitemap $sitemap
   * @param $path
   */
  protected function processPath(D8Sitemap $sitemap, $path) {
    $path = D8SitemapHelper::getCanonicalPath($path);
    $storage = $this->entityTypeManager->getStorage('d8_sitemap');
    $ids = $storage->getQuery()
      ->condition('path', $path)
      ->execute();
    if ($ids) {
      $sitemap->delete();
      $sitemap = $storage->load(reset($ids));
    }
    return $this->processSitemap($sitemap->setPath($path));
  }

  /**
   * @param \Drupal\d8_sitemap\Entity\D8Sitemap $sitemap
   * @param $path
   */
  protected function processNewEntityPath(D8Sitemap $sitemap, $path) {
    [, $entityTypeId, $entityId] = explode('!', $path);
    $entity = $this->entityTypeManager->getStorage($entityTypeId)->load($entityId);
    if ($entity) {
      return $this->processPath($sitemap, $entity->toUrl()->toString());
    }
    throw D8SitemapRemoveException::createFromSitemap($sitemap);
  }

  /**
   * @param \Drupal\d8_sitemap\Entity\D8Sitemap $sitemap
   * @param $response
   *
   * @return \Drupal\d8_sitemap\Entity\D8Sitemap|FALSE
   */
  protected function processResponse(D8Sitemap $sitemap, Response $response) {
    if ($response instanceof RedirectResponse) {
      return $this->processPath($sitemap, $response->getTargetUrl());
    }
    if ($response instanceof HtmlResponse) {
      if (intval($response->getStatusCode()) !== 200) {
        throw D8SitemapRemoveException::createFromSitemap($sitemap);
      }
    }
    elseif (!$response instanceof D8SitemapResponse) {
      throw D8SitemapRemoveException::createFromSitemap($sitemap);
    }
    return FALSE;
  }

  /**
   * @param \Drupal\d8_sitemap\Entity\D8Sitemap $sitemap
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   * @param $path
   *
   * @return bool|\Drupal\d8_sitemap\Entity\D8Sitemap|FALSE
   */
  protected function processCanonicalEntityPath(D8Sitemap $sitemap, ContentEntityInterface $entity, $path) {
    $this->checkMetatags($sitemap, $entity);
    $sitemap->setAvailableLanguages($entity->getTranslationLanguages());
    $url = $entity->toUrl();
    // clone is necessary because setOption mutates the url object.
    $unprocessedPath = (clone $url)->setOption('path_processing', FALSE)->toString();
    $processed = $url->toString();
    // Language negotiation might add a prefix, ignore that.
    $preg = '/' . preg_quote($unprocessedPath, '/') . '$/';
    if (preg_match($preg, $processed)) {
      // This entity does not have an alias or other outbound processed
      // path, these we skip.
      throw D8SitemapRemoveException::createFromSitemap($sitemap);
    }
    elseif (preg_match($preg, $path)) {
      // The unprocessed entity path was given, move over to the processed one
      // instead -- this is not the same as the unprocessed because the
      // previous if() killed that already.
      return $this->processPath($sitemap, $processed);
    }
    return FALSE;
  }

  /**
   * @param \Drupal\d8_sitemap\Entity\D8Sitemap $sitemap
   * @param \Symfony\Component\HttpFoundation\ParameterBag $attributes
   */
  protected function checkAccess(D8Sitemap $sitemap, ParameterBag $attributes) {
    // AccessAwareRouter::checkAccess() sets this.
    $accessResult = $attributes->get(AccessAwareRouterInterface::ACCESS_RESULT);
    if (!$accessResult instanceof AccessResultInterface || !$accessResult->isAllowed()) {
      throw D8SitemapRemoveException::createFromSitemap($sitemap);
    }
  }


  /**
   * @param \Drupal\d8_sitemap\Entity\D8Sitemap $sitemap
   * @param \Symfony\Component\HttpFoundation\ParameterBag $attributes
   */
  protected function checkException(D8Sitemap $sitemap, ParameterBag $attributes) {
    // HttpExceptionSubscriberBase::onException() sets this.
    if ($attributes->has('exception')) {
      throw D8SitemapRemoveException::createFromSitemap($sitemap);
    }
  }

  /**
   * @param \Drupal\Core\Url $url
   *
   * @return string
   */
  protected function getXml(RouteMatchInterface $routeMatch, array $languages) {
    $url = Url::fromRouteMatch($routeMatch)->setAbsolute();
    $xml = strtr("<url><loc>:loc</loc>\n", [':loc' => $url->toString()]);
    foreach ($languages as $language) {
      $xml .= strtr('<xhtml:link rel="alternate" hreflang=":lang" href=":href"/>:break', [
        ':href' => $url->setOption('language', $language)->toString(),
        ':lang' => $language->getId(),
        ':break' => "\n",
      ]);;
    }
    foreach ($routeMatch->getParameters() as $key => $value) {
      if ($routeMatch->getRouteName() === "entity.$key.canonical" && $value instanceof EntityChangedInterface) {
        $xml .= sprintf("<lastmod>%s</lastmod>\n", date('Y-m-d', $value->getChangedTime()));
        break;
      }
    }
    $xml .= "<changefreq>daily</changefreq></url>\n";
    return $xml;
  }

  protected function checkRobotsTxt(D8Sitemap $sitemap, $path) {
    if (!isset($this->validator)) {
      $parser = new RobotsTxtParser(file_get_contents('./robots.txt'));
      $this->validator = new RobotsTxtValidator($parser->getRules());
    }
    if (!$this->validator->isUrlAllow($path)) {
      throw D8SitemapRemoveException::createFromSitemap($sitemap);
    }
  }

  /**
   * @param \Drupal\d8_sitemap\Entity\D8Sitemap $sitemap
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   */
  protected function checkMetatags(D8Sitemap $sitemap, ContentEntityInterface $entity) {
    $tags = $this->metatagManager->tagsFromEntityWithDefaults($entity);
    // We can't reuse the url from the caller because setAbsolute mutates the
    // url object.
    $url = $entity->toUrl()->setAbsolute()->toString();
    if (isset($tags['canonical_url']) && $tags['canonical_url'] !== '[node:url]' && $tags['canonical_url'] !== $url) {
      throw D8SitemapRemoveException::createFromSitemap($sitemap);
    }
    if (isset($tags['robots'])) {
      $robots = array_map('trim', explode(',', $tags['robots']));
      if (in_array('noindex', $robots)) {
        throw D8SitemapRemoveException::createFromSitemap($sitemap);
      }
    }
  }

}
